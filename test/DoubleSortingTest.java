import static org.junit.Assert.*;
import org.junit.Test;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/** Test class.
 * @author Jaanus
 */
public class DoubleSortingTest {

   static final double delta = 0.0000001;
   double[] a, b;
   String msg = "";

   /**
    * Check whether an array is ordered.
    *
    * @param a sorted (?) array
    * @return false
    * if an array is not ordered
    */
   static boolean inOrder(double[] a) {
      if (a.length < 2)
         return true;
      for (int i = 0; i < a.length - 1; i++) {
         if (a[i] > a[i + 1])
            return false;
      }
      return true;
   }

   @Test(timeout = 1000)
   public void testTrivialArray() {
      a = new double[]{1., 3., 2.};
      b = new double[]{1., 2., 3.};
      msg = Arrays.toString(a);
      DoubleSorting.binaryInsertionSort(a);
      assertArrayEquals(msg, b, a, delta);
   }

   @Test(timeout = 1000)
   public void testRandom1000() {
      double[] a = new double[1000];
      Random generaator = new Random();
      for (int i = 0; i < a.length; i++) {
         a[i] = generaator.nextDouble() * 100.;
      }
      DoubleSorting.binaryInsertionSort(a);
      msg = " array not sorted!";
      assertTrue(msg, inOrder(a));
   }

   @Test
   public void testSpeed() {

      int i = 128000;

      double[] array = new double[i];
      double[] array2 = new double[i];
      for (int j = 0; j < i; j++) {
         array[j] = j;
         array2[j] = j;
      }

      shuffleArray(array);
      shuffleArray(array2);

//      long t0 = System.currentTimeMillis();
//      DoubleSorting.insertionSort(array);
//      long t1 = System.currentTimeMillis();
      long t10 = System.currentTimeMillis();
      DoubleSorting.binaryInsertionSort(array2);
      long t11 = System.currentTimeMillis();
//      System.out.println("insertionSort Array length: " + i + ", time spent: " + (t1-t0) + "ms");
      System.out.println("binaryInsertionSort Array length: " + i + ", time spent: " + (t11-t10) + "ms");

//      for (int i = 6300; i < 6500; i++) {
//         double[] array = new double[i];
//         double[] array2 = new double[i];
//         for (int j = 0; j < i; j++) {
//            array[j] = j;
//            array2[j] = j;
//         }
//
//         shuffleArray(array);
//         shuffleArray(array2);
//
//         long t0 = System.currentTimeMillis();
//         DoubleSorting.insertionSort(array);
//         long t1 = System.currentTimeMillis();
//         long t10 = System.currentTimeMillis();
//         DoubleSorting.binaryInsertionSort(array);
//         long t11 = System.currentTimeMillis();
//         System.out.println("insertionSort Array length: " + i + ", time spent: " + (t1-t0) + "ms");
//         System.out.println("binaryInsertionSort Array length: " + i + ", time spent: " + (t11-t10) + "ms");
//         }
      }
   // Implementing Fisher–Yates shuffle

   static void shuffleArray(double[] ar)
   {
      // If running on Java 6 or older, use `new Random()` on RHS here
      Random rnd = ThreadLocalRandom.current();
      for (int i = ar.length - 1; i > 0; i--)
      {
         int index = rnd.nextInt(i + 1);
         // Simple swap
         double a = ar[index];
         ar[index] = ar[i];
         ar[i] = a;
      }
   }

}

